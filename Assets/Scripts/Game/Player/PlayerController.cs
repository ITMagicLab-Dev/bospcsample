using Common;
using Common.BFB;

namespace Game.Player
{
    public class PlayerController
    {
        private PlayerView _view;
        private int _id => _device.Id;
        private IDevice _device;


        public void Init(PlayerView view, IDevice device)
        {
            _view = view;
            _device = device;

            _view.SetName(_id.ToString());
        }

        public void Deinit()
        {

        }

        public void UpdateData(double gameTime)
        {
            int value = _device.ReadValue();
            int fBottom = 0;
            int fTop = 100;
            // int fBottom = _device.ReadBottom(gameTime);
            // int fTop = _device.ReadTop(gameTime);
            EFrameZoneType fZoneType = _device.ReadColorGroup();
            EFrameStatus fStatus = _device.ReadFrame(gameTime, out fBottom, out fTop);

            _view.UpdateData(value, fBottom, fTop, fZoneType);
            _view.UpdateFrameStatus(fStatus != EFrameStatus.WAIT_PERIOD);
        }
    }
}