using System.Collections.Generic;
using UnityEngine;

namespace Game.Player
{
    public class PlayersManager : GameControllerBase
    {
        private List<PlayerController> _players = new List<PlayerController>();


        protected override void Init()
        {
            InitPlayers();
        }

        protected override void DeInit()
        {
            _players.ForEach(p => p.Deinit());
        }

        private void InitPlayers()
        {
            int count = gameEntry.PlayersCount;

            for (int i = 0; i < count; i++)
            {
                CreatePlayer(i);
            }
        }

        private void CreatePlayer(int id)
        {
            var player = new PlayerController();
            player.Init(
                  gameEntry.GameHUD.CreatePlayerView(id)
                , gameEntry.GetDevice(id)
            );

            _players.Add(player);
        }

        private void Update()
        {
            if (!IsInitialized) { return; }

            for (int i = 0; i < _players.Count; i++)
            {
                _players[i].UpdateData(gameEntry.GameTime);
            }
        }
    }
}